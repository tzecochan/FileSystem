package com.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import com.model.*;
import com.service.*;

public class OpenFileJFrame extends JFrame {
	
	private JTextArea textAreaOutput;
	private JMenuBar jmb;
	private JMenu jm;
	private JMenuItem jmi1, jmi2;
	
	private FAT fat;
	private File file;
	private String oldContent;
	private int length;
	private FATService fatService;
	private OpenFileTableModel oftm;
	private JTable jt;
	private JFrame jf;
	private JPanel panelOutput;
	private JScrollPane scrollpanel;

	
	private TableModel tm;
	private JTable jta;
	
	private boolean canClose = true;
	
	public OpenFileJFrame(FAT fat, FATService fatService, OpenFileTableModel oftm, JTable jt, TableModel tm, JTable jta){
		this.jf = this;
		this.fat = fat;
		this.fatService = fatService;
		this.oftm = oftm;
		this.jt = jt;
		this.tm = tm;
		this.jta = jta;
		this.file = (File)fat.getObject(); 
		textAreaOutput = new JTextArea();
		jmb = new JMenuBar();
		jm = new JMenu("操作");
		jmi1 = new JMenuItem("保存");
		jmi2 = new JMenuItem("退出");
		jmb.add(jm);
		System.out.println(file.getProperty());
		if(file.getProperty()==1) {jm.add(jmi1);}
		jm.add(jmi2);
		
		oldContent = file.getContent();
		
		textAreaOutput.setText(oldContent);
		Font x=new Font("Apple Braille",0,18);
		textAreaOutput.setFont(x);
		textAreaOutput.setLineWrap(true);
		textAreaOutput.setWrapStyleWord(true);
		panelOutput = new JPanel();
		scrollpanel= new JScrollPane(textAreaOutput);
		panelOutput.add(scrollpanel);
		scrollpanel.setPreferredSize(new Dimension(800, 550));
		
		
		
		init();
		menuItemAddListener();
	}
	
	private void init(){
		this.setResizable(false);
		this.setSize(800, 600);
		this.setTitle(file.getFileName());
		this.setLocation(200, 150);
		this.add(jmb, BorderLayout.NORTH);
		this.add(panelOutput);
		this.addWindowListener(new WindowClosingListener());
		
		this.setVisible(true);
	}
	
	private void menuItemAddListener(){
		jmi1.addActionListener(new ActionListener() {
			//保存
			@Override
			public void actionPerformed(ActionEvent e) {
				
				save();
			}
		});
		
		jmi2.addActionListener(new ActionListener() {
			//退出
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				jf.setVisible(false);
				fatService.removeOpenFile(fat);
				oftm.initData();
				jt.updateUI();
			}
		});
	}
	
	/**
	 * 保存数据
	 */
	private void save(){
		length = textAreaOutput.getText().length();
		if (length > ((File)fat.getObject()).getLength()-8){
			//添加内容的
			int num = FileSystemUtil.getNumOfFAT(length);
			if (num > 1){
				boolean boo = fatService.saveToModifyFATS2(this, num, fat);
				if (boo){
					file.setLength(length);
					file.setContent(textAreaOutput.getText());
				}
			} else {
				file.setLength(length);
				file.setContent(textAreaOutput.getText());
			}
			file.setSize(num);
			tm.initData();
			jta.updateUI();
		}
		
	}
	
	
	class WindowClosingListener extends WindowAdapter{

		@Override
		public void windowClosing(WindowEvent e) {
			if (file.getProperty()==1&&!textAreaOutput.getText().equals(file.getContent())){
				int ret = MessageUtil.showConfirmMgs(jf, "您要保留此新文稿"+file.getFileName()+"吗？");
				if (ret == 0){
					save();
				}
				fatService.removeOpenFile(fat);
				oftm.initData();
				jt.updateUI();
			}
			fatService.removeOpenFile(fat);
			oftm.initData();
			jt.updateUI();
		}
		
	}
	
}


