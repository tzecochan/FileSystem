package com.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.model.*;
import com.service.*;import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;


public class MainFrame extends JFrame {
	
	private JPanel panel, jp2, panelFAT,panelOpen,panelCmd,panelSouth,panelInp,panelC;
	private Pie3DPainter panelCha;
	private JTextField jtfC;
	private JTextArea jtfCS;
	private Tree jtr;
	private JTable jta,jta2;
	private JScrollPane jsp1, jsp2,jsp3;
//	private JMenuBar jmb;
//	private JMenu jm;
//	private JMenuItem jmi;
	private JLabel labelFAT, jl2, labelOpen,labelCMD,labeldisk,labelTitle,jtf1;
	private JButton jbCMD;
	private TableModel tm;
	private Font font=new Font("Apple SD Gothic Neo", Font.PLAIN, 15);
	private int n;
	private boolean isFile;
	private OpenFileTableModel oftm;
	
	private Map<String, DefaultMutableTreeNode> map; //�����״չʾ��·�����ͽڵ�
	private FATService fatService;
	private List<FAT> fatList;
	private int fatIndex = 0;
	
	
	public MainFrame(){
		
		oftm = new OpenFileTableModel();
		fatList = new ArrayList<FAT>();
		map = new HashMap<String, DefaultMutableTreeNode>();
		
		initService();
		//��ʼ��
		initMainFrame();
		
//		this.jmiAddListener();
		
		//�˵���
//		jm.add(jmi);
//		jmb.add(jm);
		
		//JTable
		tm = new TableModel();
		jta = new JTable(tm);
		jsp1 = new JScrollPane(jta);
		jsp1.setPreferredSize(new Dimension(384, 390));

		//JTable openfile
		jta2 = new JTable(oftm);
		jsp2 = new JScrollPane(jta2);
		jsp2.setPreferredSize(new Dimension(510, 265));
		
		jbCMDaddListener();		
		
		//chart
        panelCha=new Pie3DPainter(tm.getnotuse(), tm.getuse());
        panelCha.setPreferredSize(new Dimension(315, 280));
        panelC=new JPanel();
        panelC.add(labeldisk,BorderLayout.NORTH);
        panelC.add(panelCha,BorderLayout.SOUTH);
        panelC.setPreferredSize(new Dimension(315, 300 ));
//        
        
		//�Ϸ��Ĳ�ѯ
		jtf1.setPreferredSize(new Dimension(450, 20));
		jtf1.setText("C:");
		jp2.add(jl2);
		jp2.add(jtf1);
//		jp2.add(jb1);
		
		panelFAT.add(labelFAT);
		panelFAT.add(jsp1, BorderLayout.CENTER);
		
		panelCmd= new JPanel();
		panelCmd.setPreferredSize(new Dimension(384, 300));
		panelInp=new JPanel();
		panelInp.setPreferredSize(new Dimension(384,30));
		
		jsp3=new JScrollPane(jtfCS);
		jsp3.setPreferredSize(new Dimension(380, 230));
		jtfCS.setEditable(false);
		
		panelInp.add(jtfC, BorderLayout.WEST);
		panelInp.add(jbCMD, BorderLayout.EAST);
		panelCmd.add(labelCMD);
		panelCmd.add(panelInp);
		panelCmd.add(jsp3);
		panelOpen.add(labelOpen,BorderLayout.NORTH);
		panelOpen.add(jsp2,BorderLayout.SOUTH);
		
		//tree
//		jp2.setLayout(new FlowLayout(FlowLayout.LEFT));
//		jp2.setPreferredSize(new Dimension(1000, 30));
		panelSouth = new JPanel();
		panelFAT.setPreferredSize(new Dimension(384, 480));
		
		panelOpen.setPreferredSize(new Dimension(512, 300));
		panelSouth.setPreferredSize(new Dimension(1225, 345));
		panelSouth.add(panelCmd, BorderLayout.WEST);
		panelSouth.add(panelOpen);
		panelSouth.add(panelC);
		jta.setFont(font);
		jta2.setFont(font);
		
		panel.setLayout(new BorderLayout());
		panelSouth.add(jp2, BorderLayout.LINE_END);
		panel.add(jtr, BorderLayout.WEST);
		panel.add(panelFAT, BorderLayout.EAST);
		panel.add(panelSouth, BorderLayout.SOUTH);
//		panel.setBackground(Color.getHSBColor(243, 243, 243));
		
		
		//����MainJFrame����
		this.setTitle("模拟磁盘文件系统实现");
		this.setSize(1230, 850);
		this.setLocation(50, 50);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setLayout(new BorderLayout());
		
		this.add(labelTitle,BorderLayout.NORTH);
		this.add(panel, BorderLayout.CENTER);
		
		
		//����ͼƬ
		ImageIcon image = new ImageIcon(getClass().getResource(FileSystemUtil.imgPath));
		this.setIconImage(image.getImage());
		
		this.setVisible(true);
	}
	public static void main(String[] args) {
		new MainFrame();
	}
	
	
	
	//��ʼ����̨����
	private void initService() {
		fatService = new FATService();
		fatService.initFAT();
	}
	public void updateChart()
	{
		panelC.remove(panelCha);
		panelCha=new Pie3DPainter(tm.getnotuse(), tm.getuse());
	       panelCha.setPreferredSize(new Dimension(315, 300));
	       panelC.add(panelCha);
	       panelC.updateUI();
	}
	public void initMainFrame(){
		panel = new JPanel(); 
		jp2 = new JPanel();  
		jbCMD=new JButton("ִ执行");
		jtfC= new JTextField();
		jtfC.setPreferredSize(new Dimension(298, 25));
		jtfCS=new JTextArea();
		jtfCS.setLineWrap(true);
		jtfCS.setWrapStyleWord(true);
		jtfCS.setText("如需查看相关指令，请键入“help”，然后单击“执行”按钮");
		panelFAT = new JPanel(); 
		panelOpen = new JPanel();
		jtf1 = new JLabel();
		jtf1.setPreferredSize(new Dimension(1000,25));
		jtr = new Tree();
		labelFAT = new JLabel("文件分配表(FAT)");
		labelFAT.setFont(new Font("Apple SD Gothic Neo", Font.PLAIN, 18));
		labelOpen = new JLabel("已打开文件表");
		labelOpen.setFont(new Font("Apple SD Gothic Neo", Font.PLAIN, 18));
		labelCMD=new JLabel("命令行");
		labelCMD.setFont(new Font("Apple SD Gothic Neo", Font.PLAIN, 18));
		labeldisk=new JLabel("磁盘占用情况");
		labeldisk.setFont(new Font("Apple SD Gothic Neo", Font.PLAIN, 18));
		labelTitle=new JLabel("模拟磁盘文件系统实现");
		labelTitle.setFont(new Font("黑体", Font.ITALIC, 50));
		jl2 = new JLabel("当前路径");
	}
	
	public void jbCMDaddListener() {
		jbCMD.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				String s=jtfC.getText();
				String[] strs = editStr(s);
				List<FAT> fats;
				jtfC.setText("");
				switch (strs[0]) {
				case "help" :
					String ss=jtfCS.getText();
					jtfCS.setText(ss+"\nuser->help\n"+
					"请键入以下指令以执行阁下命令:\n"+
						"显示当前文件夹下的内容：showContent\n"+
						"新建文件：createFile FileName \n" + 
						"新建文件夹：createFolder FolderName\n"+
						"打开文件：open FileName\n"+
						"打开文件夹：cd FolderName\n"+
						"返回上一层文件夹：cd..\n"+
						"删除文件：deleteFile Name\n"+
						"删除文件夹：deleteFolder Name\n"+
						"重命名文件：renameFile OlaName NewName\n"+
						"重命名文件夹：renameFolder OlaName NewName\\n"+
						"更改文件权限：change Filename\n"+
						"帮助：help"
							);
					break;
				case "cd":
					String	path = jtf1.getText();
					fatList=fatService.getFATs(path);
					int n = fatList.size();
					int i;
					String ss2=jtfCS.getText();
					int flag=0;
					
					for(i=0;i<n;i++) {
						if(fatList.get(i).getType() == FileSystemUtil.FOLDER) {
//							System.out.println(((File)fatList.get(i).getObject()).getFileName());
							if(((Folder)fatList.get(i).getObject()).getFolderName().equals(strs[1]))
							{flag=1;
							break;
							}
						}}
					if(flag==1) {
						Folder folder = (Folder)fatList.get(fatIndex).getObject();
						 path = folder.getLocation() + "\\" + strs[1];
						jtf1.setText(path);
						fatList=fatService.getFATs(path);
						n = fatList.size();
					if(n!=0) {
					ss2=jtfCS.getText();
					jtfCS.setText(ss2+"\n当前路径为："+path+",该文件夹下有如下内容：");
					for(i=0;i<n;i++) {
						
						if(fatList.get(i).getType() == FileSystemUtil.FILE) {
							ss2=jtfCS.getText();
							if(((File)(fatList.get(i).getObject())).getProperty()==1)
								jtfCS.setText(ss2+"\n【文件】"+((File)(fatList.get(i).getObject())).getFileName()+" 【权限】读与写 【起始盘块号】"+((File)(fatList.get(i).getObject())).getDiskNum()+" 【大小】"+(((File)(fatList.get(i).getObject())).getLength()+8));
								else 
									jtfCS.setText(ss2+"\n【文件】"+((File)(fatList.get(i).getObject())).getFileName()+" 【权限】只读 【起始盘块号】"+((File)(fatList.get(i).getObject())).getDiskNum()+" 【大小】"+(((File)(fatList.get(i).getObject())).getLength()+8));
						}
						else if(fatList.get(i).getType() == FileSystemUtil.FOLDER) {
							ss2=jtfCS.getText();
							jtfCS.setText(ss2+"\n【文件夹】"+((Folder)(fatList.get(fatIndex).getObject())).getFolderName()+"【起始盘块号】"+((Folder)(fatList.get(i).getObject())).getDiskNum());}
						
						
					}
//					ss2=jtfCS.getText();
//					jtfCS.setText(ss2+"\n����ʹ��ͼ�ν������ֶ�ˢ����ʹ�á�");
					}
					else {
						ss2=jtfCS.getText();
					jtfCS.setText(ss2+"\n抱歉，当前文件夹下无内容。");}}
					else {
						ss2=jtfCS.getText();
						jtfCS.setText(ss2+"\n抱歉，当前路径下找不到一个叫“"+strs[1]+"”的文件夹。");
					}
					break;
				case "showContent":
					 path = jtf1.getText();
					fatList=fatService.getFATs(path);
				 n = fatList.size();
				 ss2=jtfCS.getText();
				if(n!=0) {
					jtfCS.setText(ss2+"\n当前路径为："+path+",该文件夹下有如下内容：");
				for(i=0;i<n;i++) {
					if(fatList.get(i).getType() == FileSystemUtil.FILE) {
					ss2=jtfCS.getText();
					if(((File)(fatList.get(i).getObject())).getProperty()==1)
						jtfCS.setText(ss2+"\n【文件】"+((File)(fatList.get(i).getObject())).getFileName()+" 【权限】读与写 【起始盘块号】"+((File)(fatList.get(i).getObject())).getDiskNum()+" 【大小】"+(((File)(fatList.get(i).getObject())).getLength()+8));
						else 
							jtfCS.setText(ss2+"\n【文件】"+((File)(fatList.get(i).getObject())).getFileName()+" 【权限】只读 【起始盘块号】"+((File)(fatList.get(i).getObject())).getDiskNum()+" 【大小】"+(((File)(fatList.get(i).getObject())).getLength()+8));
				}
				else if(fatList.get(i).getType() == FileSystemUtil.FOLDER) {
					ss2=jtfCS.getText();
					jtfCS.setText(ss2+"\n【文件夹】"+((Folder)(fatList.get(fatIndex).getObject())).getFolderName()+"【起始盘块号】"+((Folder)(fatList.get(i).getObject())).getDiskNum());}
				
				
			}
				}
				else {
					ss2=jtfCS.getText();
					jtfCS.setText(ss2+"\n抱歉，当前文件夹下无内容。");}
					break;
				case "createFile":
					int index = fatService.createFile(jtf1.getText(),strs[1]);
					if (index == FileSystemUtil.ERROR){
						ss2=jtfCS.getText();
						jtfCS.setText(ss2+"\n抱歉，磁盘空间已满，无法创建新文件。");
					} else {
						tm.initData();
						String ss3=jtfCS.getText();
						jtfCS.setText(ss3+"\n文件“"+strs[1]+"”创建成功，请刷新面板查看。");

					}
					jta.updateUI();
					break;	
				case "createFolder":
					int index1 = fatService.createFolder(jtf1.getText(),strs[1]);
					if (index1 == FileSystemUtil.ERROR){
						ss2=jtfCS.getText();
						jtfCS.setText(ss2+"\n抱歉，磁盘空间已满，无法创建新文件。");
					} else {
						FAT fat = fatService.getFAT(index1);
						DefaultMutableTreeNode node = new DefaultMutableTreeNode((Folder)(fat.getObject()));
						map.put(jtf1.getText() + "\\" + ((Folder)(fat.getObject())).getFolderName(), node);
						DefaultMutableTreeNode nodeParent = map.get(jtf1.getText());
						nodeParent.add(node);
						tm.initData();
						jta.updateUI();
						panelCha.removeAll();updateChart();
						jtr.updateUI();
						String ss3=jtfCS.getText();
						jtfCS.setText(ss3+"\n文件夹“\"+strs[1]+\"”创建成功，请刷新面板查看。");
					}
					jta.updateUI();
					break;
				case "open":
					path = jtf1.getText();
						fatList=fatService.getFATs(path);
						n = fatList.size();
					 flag=0;
				
					for(i=0;i<n;i++) {
						if(fatList.get(i).getType() == FileSystemUtil.FILE) {
							System.out.println(((File)fatList.get(i).getObject()).getFileName());
							if(((File)fatList.get(i).getObject()).getFileName().equals(strs[1]))
							{flag=1;
							break;
							}
						}}
					if(flag==1) {
						if (fatService.getOpenFiles().getFiles().size() < FileSystemUtil.num){
							if (fatService.checkOpenFile(fatList.get(i))){
								String ss3=jtfCS.getText();
								jtfCS.setText(ss3+"\n该文件已打开。");
								break;
							}
							fatService.addOpenFile(fatList.get(i), FileSystemUtil.flagWrite);
							oftm.initData();
							jta2.updateUI();
							new OpenFileJFrame(fatList.get(i), fatService, oftm, jta2, tm, jta);
						} else {
							String ss3=jtfCS.getText();
							jtfCS.setText(ss3+"\n抱歉，最多同时打开五个文件。");
						}}
					else {
						String ss3=jtfCS.getText();
					jtfCS.setText(ss3+"\n抱歉，找不到一个叫“"+strs[1]+"”的文件。");}
					jta2.updateUI();
					break;
				case "cd..":
					String path1 = jtf1.getText();
			
					if((path1.charAt(path1.length()-1))==':')
					{ String ss4=jtfCS.getText();
					jtfCS.setText(ss4+"\n抱歉，已经没有上层文件夹了。");
					}
					else {
					while(true) {
//						System.out.println(path.charAt(path.length()-1));
						if((path1.charAt(path1.length()-1))!='\\')
							path1=path1.substring(0, path1.length()-1);
						else break;	}
					path1=path1.substring(0, path1.length()-1);
					jtf1.setText(path1);
					fatList=fatService.getFATs(path1);
					n = fatList.size();
				if(n!=0) {
				ss2=jtfCS.getText();
				jtfCS.setText(ss2+"\n当前路径为："+path1+",该文件夹下有以下内容：");
				for(i=0;i<n;i++) {
					
					if(fatList.get(i).getType() == FileSystemUtil.FILE) {
						ss2=jtfCS.getText();
						if(((File)(fatList.get(i).getObject())).getProperty()==1)
							jtfCS.setText(ss2+"\n【文件】"+((File)(fatList.get(i).getObject())).getFileName()+" 【权限】读与写 【起始盘块号】"+((File)(fatList.get(i).getObject())).getDiskNum()+" 【大小】"+(((File)(fatList.get(i).getObject())).getLength()+8));
							else 
								jtfCS.setText(ss2+"\n【文件】"+((File)(fatList.get(i).getObject())).getFileName()+" 【权限】只读 【起始盘块号】"+((File)(fatList.get(i).getObject())).getDiskNum()+" 【大小】"+(((File)(fatList.get(i).getObject())).getLength()+8));
					}
					else if(fatList.get(i).getType() == FileSystemUtil.FOLDER) {
						ss2=jtfCS.getText();
						jtfCS.setText(ss2+"\n【文件夹】"+((Folder)(fatList.get(fatIndex).getObject())).getFolderName()+"【起始盘块号】"+((Folder)(fatList.get(i).getObject())).getDiskNum());}
					
					
				}
				}
				else {
					ss2=jtfCS.getText();
				jtfCS.setText(ss2+"\n当前路径为："+path1+"，该文件夹下无内容。");}}
					break;
				case "deleteFile":	
					path = jtf1.getText();
					fatList=fatService.getFATs(path);
					n = fatList.size();
				 flag=0;
			
				for(i=0;i<n;i++) {
					if(fatList.get(i).getType() == FileSystemUtil.FILE) {
						System.out.println(((File)fatList.get(i).getObject()).getFileName());
						if(((File)fatList.get(i).getObject()).getFileName().equals(strs[1]))
						{flag=1;
						break;
						}
					}}
				if(flag==1) {
					i=fatService.delete(panel, fatList.get(i), map);
					if(i==1) {
					String ss3=jtfCS.getText();
					jtfCS.setText(ss3+"\n成功删除一个叫“"+strs[1]+"”的文件，请刷新面板查看。");}
					}
				else {
					String ss3=jtfCS.getText();
				jtfCS.setText(ss3+"\n抱歉，无法删除。");}
				jta.updateUI();
				break;
				case "deleteFolder":
					path = jtf1.getText();
					fatList=fatService.getFATs(path);
					n = fatList.size();
					ss2=jtfCS.getText();
					flag=0;
					
					for(i=0;i<n;i++) {
						if(fatList.get(i).getType() == FileSystemUtil.FOLDER) {
							if(((Folder)fatList.get(i).getObject()).getFolderName().equals(strs[1]))
							{flag=1;
							break;
							}
						}}
					if(flag==1) {
						 i=fatService.delete(panel, fatList.get(fatIndex), map);
						if(i==1) {
							String ss3=jtfCS.getText();
							jtfCS.setText(ss3+"\n成功删除“"+strs[1]+"”文件夹，请刷新面板查看。");}
							
					}
					else {
						ss2=jtfCS.getText();
						jtfCS.setText(ss2+"\n抱歉，无法删除。");
					}
					jta.updateUI();
					break;
				case "renameFile":
					path = jtf1.getText();
					fatList=fatService.getFATs(path);
					n = fatList.size();
				 flag=0;
			
				for(i=0;i<n;i++) {
					if(fatList.get(i).getType() == FileSystemUtil.FILE) {
						System.out.println(((File)fatList.get(i).getObject()).getFileName());
						if(((File)fatList.get(i).getObject()).getFileName().equals(strs[1]))
						{flag=1;
						break;
						}
					}}
				for(int j=0;j<n;j++) {
					if(fatList.get(j).getType() == FileSystemUtil.FILE) {
						System.out.println(((File)fatList.get(j).getObject()).getFileName());
						if(((File)fatList.get(j).getObject()).getFileName().equals(strs[2]))
						{flag=2;
						break;
						}
					}}
				if(flag==1) {
					isFile = true;
					File file = (File)fatList.get(i).getObject();
					String oldName=strs[1];
					String rename=strs[2];
					String oldPath = file.getLocation() + "\\" + oldName;
					path = file.getLocation();
					if (rename != null && rename != "" && !rename.equals(oldName)){
						path1 = ((File)fatList.get(i).getObject()).getLocation() + "\\" + rename;
						if (rename.contains("$") || rename.contains(".") || rename.contains("/")){
							String ss3=jtfCS.getText();
							jtfCS.setText(ss3+"\n抱歉，新文件名中含有\"$\",\".\",\"/\"，不能修改。");
							return ;
						}
						((File)fatList.get(i).getObject()).setFileName(rename);
						String ss3=jtfCS.getText();
						jtfCS.setText(ss3+"\n修改成功，请刷新面板查看。");
				}}
				else if(flag==0) {
					String ss3=jtfCS.getText();
				jtfCS.setText(ss3+"\n抱歉，当前文件夹下找不到叫做“"+strs[2]+"”的文件。");}
				else if(flag==2) {
					String ss3=jtfCS.getText();
					jtfCS.setText(ss3+"\n抱歉，新的文件名与当前文件夹中现有文件名一样，无法修改。");
				}
					break;
				case"renameFolder":
					path = jtf1.getText();
					fatList=fatService.getFATs(path);
					n = fatList.size();
					ss2=jtfCS.getText();
					flag=0;
					
					for(i=0;i<n;i++) {
						if(fatList.get(i).getType() == FileSystemUtil.FOLDER) {
							if(((Folder)fatList.get(i).getObject()).getFolderName().equals(strs[1]))
							{flag=1;
							break;
							}
						}}
					for(int j=0;j<n;j++) {
						if(fatList.get(j).getType() == FileSystemUtil.FOLDER) {
							if(((Folder)fatList.get(j).getObject()).getFolderName().equals(strs[2]))
							{flag=2;
							break;
							}
						}}
					if(flag==1) {
						isFile = false;
						Folder folder = (Folder)fatList.get(i).getObject();
						String oldName = strs[1];
						String oldPath = folder.getLocation() + "\\" + oldName;
						path = folder.getLocation();
						String rename = strs[2];
						if (rename != null &&( rename != "" && !rename.equals(oldName))){
							path1 = ((Folder)fatList.get(i).getObject()).getLocation() + "\\" + rename;
							
							if (rename.contains("$") || rename.contains(".") || rename.contains("/")){
								String ss3=jtfCS.getText();
								jtfCS.setText(ss3+"\n抱歉，新文件名中含有\\\"$\\\",\\\".\\\",\\\"/\\\"，不能修改。");
								return ;
							}
							
							((Folder)fatList.get(i).getObject()).setFolderName(rename);
							String ss3=jtfCS.getText();
							jtfCS.setText(ss3+"\n修改成功，请刷新面板查看。");
						}
					}
					else if(flag==0) {
						String ss3=jtfCS.getText();
					jtfCS.setText(ss3+"\n抱歉，当前文件夹下找不到叫做“"+strs[2]+"”的文件夹。");}
					else if(flag==2) {
						String ss3=jtfCS.getText();
						jtfCS.setText(ss3+"\n\\n抱歉，新的文件夹名与当前文件夹中现有文件夹名一样，无法修改");
					}
					break;
				case"change":
					path = jtf1.getText();
					fatList=fatService.getFATs(path);
					n = fatList.size();
				 flag=0;
			
				for(i=0;i<n;i++) {
					if(fatList.get(i).getType() == FileSystemUtil.FILE) {
						System.out.println(((File)fatList.get(i).getObject()).getFileName());
						if(((File)fatList.get(i).getObject()).getFileName().equals(strs[1]))
						{flag=1;
						break;
						}
					}}
				if(flag==1) {
					int pp=((File)(fatList.get(i).getObject())).getProperty();
					if(pp==1) {
									((File)(fatList.get(i).getObject())).setProperty(0);
									String ss3=jtfCS.getText();
									jtfCS.setText(ss3+"\n文件已修改权限为只读。");
							

					}
					else 
					{

						((File)(fatList.get(i).getObject())).setProperty(1);
						String ss3=jtfCS.getText();
						jtfCS.setText(ss3+"\n文件已修改权限为读与写。");
				
						
					}
				}
				else {
					String ss3=jtfCS.getText();
				jtfCS.setText(ss3+"\n抱歉，找不到叫“"+strs[1]+"”的文件。");}
				
					
					break;
				default:
					String ss1=jtfCS.getText();
					jtfCS.setText(ss1+"\n无法识别指令。");
					break;
				}
				}
				
			
		});
	}
	public static String[] editStr(String str) {
		Pattern pattern = Pattern.compile("([a-zA-Z0-9.\\\\/]*) *");// ���ݿո�ָ���������
		Matcher m = pattern.matcher(str);
		ArrayList<String>  list = new ArrayList<String>();
		while(m.find()){
			list.add(m.group(1));
		}
		String[] strs = list.toArray(new String[list.size()]);
		
		for (int i = 1; i < strs.length; i++) { // �жϳ���������ÿһ���������Ƿ��� "."
			int j = strs[i].indexOf(".");
			if (j != -1) { // ������"." �����и� ȡǰ������Ϊ�ļ���
				String[] index = strs[i].split("\\."); // ʹ��ת���ַ�"\\."
				strs[i] = index[0];
			}
		} 
		return strs;
	}
	/*
	 * ��
	 */
	public class Tree extends JPanel {
		
		private static final long serialVersionUID = 2352829445429133249L;
		private JTree tree;
		private JScrollPane jsp1, jsp2;
		private JSplitPane jsp;
		private JPanel jp1;
		private MyJLabel[] jLabel;
		private JPopupMenu create_menu, file_menu,folder_menu;
		private JMenuItem create_file_menuitem, create_folder_menuitem, open_file_menuitem, rename_file_menuitem, delete_file_menuitem, modify_file_menuitem,open_folder_menu,delete_folder_menu,rename_folder_menuitem,refresh_menu;
		private DefaultMutableTreeNode node1;

		public Tree() {
			
			this.initMenuItem();
			this.initFileMenu();
			this.initFolderMenu();
			this.menuItemAddListener();
			
			this.initTree();
			this.treeAddListener();
			this.jPanelAddListener();
			
			jp1.setLayout(new FlowLayout(FlowLayout.LEFT));
			jp1.setBackground(Color.WHITE);
			jp1.add(create_menu);
			
			jsp2 = new JScrollPane(jp1);
			jsp2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			jsp2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			jsp2.setPreferredSize(new Dimension(571,415));
			jsp2.setBackground(Color.white);
			jsp2.setViewportView(jp1);
			
			jsp1.setPreferredSize(new Dimension(245, 415));
			
			jsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,jsp1,jsp2);
			
			jsp.setDividerSize(0);
			jsp.setDividerLocation(200);
			jsp.setEnabled(false);
			
			this.add(jsp);
		}
		
		/**
		 * ��ʼ����
		 */
		private void initTree() {
			node1 = new DefaultMutableTreeNode(new Disk("C"));
			map.put("C:", node1);
			jp1 = new JPanel();
			tree = new JTree(node1);
			jsp1 = new JScrollPane(tree);
		}
		
		private void treeAddListener(){
			tree.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					//�ı��ַ��·��
					TreePath path = tree.getSelectionPath();
					if (path != null){
						String pathStr = path.toString().replace("[", "").replace("]", "").replace(",", "\\").replace(" ", "").replaceFirst("C", "C:");
						jtf1.setText(pathStr);
						
						//����jp1
						jp1.removeAll();
						addJLabel(fatService.getFATs(pathStr), pathStr);
						jp1.updateUI();
						panelCha.removeAll();updateChart();
					}
				}
			});
		}
		
		/**
		 * ����������JLabel
		 */
		private void addJLabel(List<FAT> fats, String path){
			fatList = fats;
			isFile = true;
			n = fats.size();
			jp1.setPreferredSize(new Dimension(482, FileSystemUtil.getHeight(n)));
			jLabel = new MyJLabel[n];
			for (int i=0; i<n; i++){
				if (fats.get(i).getIndex() == FileSystemUtil.END){
					if (fats.get(i).getType() == FileSystemUtil.FOLDER){
						isFile = false;
						jLabel[i] = new MyJLabel(isFile, ((Folder)fats.get(i).getObject()).getFolderName());
					} else {
						isFile = true;
						jLabel[i] = new MyJLabel(isFile, ((File)fats.get(i).getObject()).getFileName());
					}
					jp1.add(jLabel[i]);
//					jLabel[i].add(file_menu);
					jLabel[i].addMouseListener(new MouseListener() {
						
						@Override
						public void mouseReleased(MouseEvent e) {
							
						}
						
						@Override
						public void mousePressed(MouseEvent e) {
							for (int j=0; j<n; j++){
								if (e.getSource() == jLabel[j] && ((e.getModifiers() & InputEvent.BUTTON3_MASK)!=0)){
									if(jLabel[j].type)
									file_menu.show(jLabel[j], e.getX(), e.getY());
									else 
									folder_menu.show(jLabel[j], e.getX(), e.getY());
	
								}
							}
						}
						
						@Override
						public void mouseExited(MouseEvent e) {
							for (int j=0; j<n; j++){
								if (e.getSource() == jLabel[j]){
									fatIndex = j;
									if (jLabel[j].type){
										jLabel[j].setIcon(new ImageIcon(getClass().getResource(FileSystemUtil.filePath)));
									} else {
										jLabel[j].setIcon(new ImageIcon(getClass().getResource(FileSystemUtil.folderPath)));
									}
								}
							}
						}
						
						@Override
						public void mouseEntered(MouseEvent e) {
							for (int j=0; j<n; j++){
								if (e.getSource() == jLabel[j]){
									fatIndex = j;
									if (jLabel[j].type){
										jLabel[j].setIcon(new ImageIcon(getClass().getResource(FileSystemUtil.file1Path)));
										if(((File)(fatList.get(fatIndex).getObject())).getProperty()==1)
										jLabel[j].setToolTipText("【文件名】"+((File)(fatList.get(fatIndex).getObject())).getFileName()+"【权限】读与写【起始盘块号】"+((File)(fatList.get(fatIndex).getObject())).getDiskNum()+"【大小】"+(((File)(fatList.get(fatIndex).getObject())).getLength()+8));
										else 
										jLabel[j].setToolTipText("【文件名】"+((File)(fatList.get(fatIndex).getObject())).getFileName()+"【权限】只读【起始盘块号】"+((File)(fatList.get(fatIndex).getObject())).getDiskNum()+"【大小】"+(((File)(fatList.get(fatIndex).getObject())).getLength()+8));
		
									} else {
										jLabel[j].setIcon(new ImageIcon(getClass().getResource(FileSystemUtil.folder1Path)));
										jLabel[j].setToolTipText("【文件夹名】"+((Folder)(fatList.get(fatIndex).getObject())).getFolderName());
									}
								}
							}
						}
						
						@Override
						public void mouseClicked(MouseEvent e) {
							if (e.getClickCount() == 2){
								if (fatList.get(fatIndex).getType() == FileSystemUtil.FILE){
									//�ļ�
									if (fatService.getOpenFiles().getFiles().size() < FileSystemUtil.num){
										if (fatService.checkOpenFile(fatList.get(fatIndex))){
											MessageUtil.showErrorMgs(jp1, "文件已打开");
											return;
										}
										fatService.addOpenFile(fatList.get(fatIndex), FileSystemUtil.flagWrite);
										oftm.initData();
										jta2.updateUI();
										panelCha.removeAll();updateChart();
										new OpenFileJFrame(fatList.get(fatIndex), fatService, oftm, jta2, tm, jta);
									} else {
										MessageUtil.showErrorMgs(jp1, "已打开5个文件，无法再打开新文件。");
									}
									
								} else {
									//�ļ���
									Folder folder = (Folder)fatList.get(fatIndex).getObject();
									String path = folder.getLocation() + "\\" + folder.getFolderName();
									
									jp1.removeAll();
									addJLabel(fatService.getFATs(path), path);
									jp1.updateUI();
									panelCha.removeAll();updateChart();
									jtf1.setText(path);
								}
							}
						}
					});
				}
			}
		}
		
		/**
		 * �������Ӽ�����
		 */
		private void jPanelAddListener() {
			//����Ҽ�ʱ���¼�
			jp1.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					int mods = e.getModifiers();
					if ((mods&InputEvent.BUTTON3_MASK) != 0){
						create_menu.show(jp1, e.getX(), e.getY());
					}
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					
				}
			});			
		}

		//��ʼ���Ҽ��˵�
		public void initMenuItem(){
			create_menu = new JPopupMenu();
			create_file_menuitem = new JMenuItem("新建文件");
			create_folder_menuitem = new JMenuItem("新建文件夹");
			refresh_menu=new JMenuItem("刷新");
			create_menu.add(create_file_menuitem);
			create_menu.add(create_folder_menuitem);
			create_menu.add(refresh_menu);
		}
		
		public void initFileMenu(){
			file_menu = new JPopupMenu();
			open_file_menuitem = new JMenuItem("打开");
			rename_file_menuitem = new JMenuItem("重命名");
			delete_file_menuitem = new JMenuItem("删除");
			modify_file_menuitem = new JMenuItem("更改文件权限");
			file_menu.add(open_file_menuitem);
			file_menu.add(rename_file_menuitem);
			file_menu.add(delete_file_menuitem);
			file_menu.add(modify_file_menuitem);
		}
		
		private void initFolderMenu() {
			folder_menu=new JPopupMenu();
			open_folder_menu=new JMenuItem("打开");
			rename_folder_menuitem = new JMenuItem("重命名");
			delete_folder_menu=new JMenuItem("删除");
			folder_menu.add(open_folder_menu);
			folder_menu.add(rename_folder_menuitem);
			folder_menu.add(delete_folder_menu);
			
		}
		
		/**
		 * ����Ҽ�ѡ����Ӽ�����
		 */
		public void menuItemAddListener(){
			create_file_menuitem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = fatService.createFile(jtf1.getText(),"rdm");
					if (index == FileSystemUtil.ERROR){
						MessageUtil.showErrorMgs(jp1, "抱歉，磁盘空间已满，无法新建文件。");
					} else {
//						DefaultMutableTreeNode node = new DefaultMutableTreeNode((File)(fat.getObject()));
//						map.put(jtf1.getText() + "\\" + ((File)(fat.getObject())).getFileName(), node);
//						DefaultMutableTreeNode nodeParent = map.get(jtf1.getText());
//						nodeParent.add(node);
						tree.updateUI();
						panelCha.removeAll();updateChart();
						tm.initData();
						jta.updateUI();
						panelCha.removeAll();updateChart();
						jp1.removeAll();
						addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
						jp1.updateUI();
					}
				}
			});
			create_folder_menuitem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = fatService.createFolder(jtf1.getText(),"rdm");
					if (index == FileSystemUtil.ERROR){
						MessageUtil.showErrorMgs(jp1, "抱歉，磁盘空间已满，无法新建文件夹。");
					} else {
						FAT fat = fatService.getFAT(index);
						DefaultMutableTreeNode node = new DefaultMutableTreeNode((Folder)(fat.getObject()));
						map.put(jtf1.getText() + "\\" + ((Folder)(fat.getObject())).getFolderName(), node);
						DefaultMutableTreeNode nodeParent = map.get(jtf1.getText());
						nodeParent.add(node);
						tree.updateUI();
						tm.initData();
						jta.updateUI();
						panelCha.removeAll();updateChart();
						jp1.removeAll();
						addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
						jp1.updateUI();
					}
				}
			
			});
			open_file_menuitem.addActionListener(new ActionListener() {
				//��
				@Override
				public void actionPerformed(ActionEvent e){
					if (fatList.get(fatIndex).getType() == FileSystemUtil.FILE){
						//�ļ�
						if (fatService.getOpenFiles().getFiles().size() < FileSystemUtil.num){
							if (fatService.checkOpenFile(fatList.get(fatIndex))){
								MessageUtil.showErrorMgs(jp1, "已打开该文件。");
								return;
							}
							fatService.addOpenFile(fatList.get(fatIndex), FileSystemUtil.flagWrite);
							oftm.initData();
							jta2.updateUI();
							
							updateChart();
					        System.out.println("notuse"+tm.getnotuse());
					        System.out.println("use"+tm.getuse());
							
							
							new OpenFileJFrame(fatList.get(fatIndex), fatService, oftm, jta2, tm, jta);
						} else {
							MessageUtil.showErrorMgs(jp1, "已打开五个文件，无法再打开新文件。");
						}
						
					} 
				}
			});
			rename_file_menuitem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					new ShowRenameDialog(jp1, fatList.get(fatIndex), map, fatService);
					tree.updateUI();
					tm.initData();
					jta.updateUI();
					panelCha.removeAll();updateChart();
					jp1.removeAll();
					addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
					jp1.updateUI();
				}
			});
			delete_file_menuitem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int i = MessageUtil.showConfirmMgs(jp1, "是否需要删除该文件？");
					if (i==0){
						fatService.delete(jp1, fatList.get(fatIndex), map);
						
						tree.updateUI();
						tm.initData();
						jta.updateUI();
						panelCha.removeAll();updateChart();
						jp1.removeAll();
						addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
						jp1.updateUI();
					}
					
				}
			});
			modify_file_menuitem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int pp=((File)(fatList.get(fatIndex).getObject())).getProperty();
					if(pp==1) {
						int i=MessageUtil.showConfirmMgs(jp1, "当前文件权限为“读与写”，是否修改为“只读”？");
								if(i==0) {
									((File)(fatList.get(fatIndex).getObject())).setProperty(0);
								}
					}
					else 
					{int i=MessageUtil.showConfirmMgs(jp1, "当前文件权限为“只读”，是否修改为“读与写”？");
					if(i==0) {
						((File)(fatList.get(fatIndex).getObject())).setProperty(1);
					}
						
					}
				}
			});
			open_folder_menu.addActionListener(new ActionListener() {
				//��
				@Override
				public void actionPerformed(ActionEvent e){
					
						Folder folder = (Folder)fatList.get(fatIndex).getObject();
						String path = folder.getLocation() + "\\" + folder.getFolderName();
						
						jp1.removeAll();
						addJLabel(fatService.getFATs(path), path);
						jp1.updateUI();
						jtf1.setText(path);
						panelCha.removeAll();updateChart();
				}
				
			});
			rename_folder_menuitem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					new ShowRenameDialog(jp1, fatList.get(fatIndex), map, fatService);
					tree.updateUI();
					tm.initData();
					jta.updateUI();
					panelCha.removeAll();updateChart();
					jp1.removeAll();
					addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
					jp1.updateUI();
				}
			});
			delete_folder_menu.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int i = MessageUtil.showConfirmMgs(jp1, "是否删除该文件夹？");
					if (i==0){
						fatService.delete(jp1, fatList.get(fatIndex), map);
						
						tree.updateUI();
						tm.initData();
						jta.updateUI();
						panelCha.removeAll();updateChart();
						jp1.removeAll();
						addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
						jp1.updateUI();
					}
					
				}
			});
			refresh_menu.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					tree.updateUI();
					tm.initData();
					jta.updateUI();
					panelCha.removeAll();updateChart();
					jp1.removeAll();
					addJLabel(fatService.getFATs(jtf1.getText()), jtf1.getText());
					jp1.updateUI();
				}
				
			});
		}
		
	}
	}
