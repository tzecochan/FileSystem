package com.frame;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import com.model.*;
import com.service.*;
public class OpenFileTableModel extends AbstractTableModel {

	private Vector<String> columnNames;
	private Vector<Vector<String>> rowDatas;
	private FATService fatService;
	
	public OpenFileTableModel(){
		fatService = new FATService();
		initData();
	}
	
	public void initData(){
		columnNames = new Vector<String>();
		columnNames.add("文件名称");
		columnNames.add("文件打开方式");
		columnNames.add("文件起始盘块号");
		columnNames.add("文件路径");
		columnNames.add("文件大小");
		
		Vector<String> vc = null;
		rowDatas = new Vector<Vector<String>>();
		OpenFiles openFiles = fatService.getOpenFiles();
		for (int i=0; i<FileSystemUtil.num; i++){
			vc = new Vector<String>();
			if(i < openFiles.getFiles().size()){
				vc.add(openFiles.getFiles().get(i).getFile().getFileName());
				vc.add(openFiles.getFiles().get(i).getFile().getProperty()==0 ? "只读" : "读与写");
				vc.add(openFiles.getFiles().get(i).getFile().getDiskNum() + "");
				vc.add(openFiles.getFiles().get(i).getFile().getLocation());
				vc.add(Integer.toString(openFiles.getFiles().get(i).getFile().getLength()+8));
			} else {
				vc.add("");
				vc.add("");
				vc.add("");
				vc.add("");
				vc.add("");
			}
			rowDatas.add(vc);
		}
	}
	
	@Override
	public int getRowCount() {
		return 5;
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames.get(column);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return rowDatas.get(rowIndex).get(columnIndex);
	}


}
