package com.frame;

import java.util.List;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import com.model.FAT;
import com.service.FATService;

public class TableModel extends AbstractTableModel {

	private Vector<String> columnNames;
	private Vector<Vector<Integer>> rowDatas;
	private FATService fatService;
	private int index = 0;
	private int use=0;
	private int notuse=0;
	
	public TableModel(){
		fatService = new FATService();
		initData();
	}
	
	public void initData(){
		columnNames = new Vector<String>();
		columnNames.add("磁盘块");
		columnNames.add("值");
		rowDatas = new Vector<Vector<Integer>>();
		Vector<Integer> vs = null;
		FAT[] list = fatService.getMyFAT();
		use=0;
		notuse=0;
		for (int i=0; i<128; i++){
			vs = new Vector<Integer>();
			if (list[i] != null){
				vs.add(i);
				vs.add(list[i].getIndex());
				use++;
			} else {
				vs.add(i);
				vs.add(0);
				notuse++;
			}
			rowDatas.add(vs);
		}
		
	}
	
	public int getuse() {
		return use;
	}
	public int getnotuse() {
		return notuse;
	}
	
	@Override
	public int getRowCount() {
		return 128;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}
	
	@Override
	public String getColumnName(int column) {
		return columnNames.get(column);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return rowDatas.get(rowIndex).get(columnIndex);
	}

}
