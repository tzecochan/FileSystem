package com.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.DataFormatException;

public class Folder {


	private String folderName; 
	private int diskNum;
	private String type;
	
	private boolean hasChild;
	private int numOfFAT;
	
	//查看的属性
	private String location; //位置

	private Date createTime; //创建时间

	
	public Folder(String folderName) {
		super();
		this.folderName = folderName;
	}
	
	public Folder(String folderName, String location, int diskNum){
		super();
		this.folderName = folderName;
		this.location = location;
		this.createTime = new Date();
		this.diskNum = diskNum;
		this.type = "Folder";
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}


	public int getDiskNum() {
		return diskNum;
	}

	public void setDiskNum(int diskNum) {
		this.diskNum = diskNum;
	}

	public boolean isHasChild() {
		return hasChild;
	}

	public void setHasChild(boolean hasChild) {
		this.hasChild = hasChild;
	}

	public int getNumOfFAT() {
		return numOfFAT;
	}

	public void setNumOfFAT(int numOfFAT) {
		this.numOfFAT = numOfFAT;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}


	public String getCreateTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss");
		return format.format(createTime);
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	@Override
	public String toString() {
		return folderName;
	}
}
